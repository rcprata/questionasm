org 100h

    mov bx, 0
    mov ah, 0
    
    back:
        add bx, ax
        dec cx
        
    jnz back  
    
    mov ax, bx
    
ret