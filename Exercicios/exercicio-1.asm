org 100h

    sub ax, bx ;sub gera overflow e pode
               ;ativar cf (carry se ax < bx)
               
    sbb dx, cx
    
    jnz cond ;em sbb nao acontece overflow 
             ;logo qualquer valor maior
             ;subtraido dele ativa a flag zf                     
                                          
    mov ax, 00h
    mov dx, 00h  
    
    
    cond: ;condicao de que dx > cx (nada
          ;acontece

ret 
     